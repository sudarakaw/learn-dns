use crate::Result;

const MAX_JUMPS: u8 = 5;

pub struct BytePacketBuffer {
    pub buf: [u8; 512],
    pos: usize,
}

impl BytePacketBuffer {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn pos(&self) -> usize {
        self.pos
    }

    /// Move read/write position of the buffer `steps` bytes forward.
    pub fn step(&mut self, steps: usize) -> Result<()> {
        self.pos += steps;

        Ok(())
    }

    /// Move read/write position of the buffer to exact position (byte).
    fn seek(&mut self, pos: usize) -> Result<()> {
        self.pos = pos;

        Ok(())
    }

    /// Read a single byte from the current position & move the position one
    /// steps forward
    fn read(&mut self) -> Result<u8> {
        if self.pos >= 512 {
            return Err("End of buffer".into());
        }

        let res = self.buf[self.pos];

        self.pos += 1;

        Ok(res)
    }

    /// Read a single byte at the given position without changing the buffer
    /// possition.
    fn get(&self, pos: usize) -> Result<u8> {
        if pos >= 512 {
            return Err("End of buffer".into());
        }

        Ok(self.buf[pos])
    }

    /// Read range of bytes without changing the buffer possition.
    pub fn get_range(&self, start: usize, len: usize) -> Result<&[u8]> {
        if start + len >= 512 {
            return Err("End of buffer".into());
        }

        Ok(&self.buf[start..start + len])
    }

    /// Read two bytes (word) and stepp two steps forward.
    pub fn read_u16(&mut self) -> Result<u16> {
        let res = ((self.read()? as u16) << 8) | (self.read()? as u16);

        Ok(res)
    }

    /// Read four bytes (word) and stepp four steps forward.
    pub fn read_u32(&mut self) -> Result<u32> {
        let res = ((self.read()? as u32) << 24)
            | ((self.read()? as u32) << 16)
            | ((self.read()? as u32) << 8)
            | (self.read()? as u32);

        Ok(res)
    }

    /// Read a qname
    ///
    /// The tricky part: Reading domain names, taking labels into consideration.
    /// Will take something like [3]www[6]google[3]com[0] and append
    /// www.google.com to outstr.
    pub fn read_qname(&mut self, outstr: &mut String) -> Result<()> {
        // Keep track of decoded lables in a vector to join with delimiter
        // later.
        let mut labels: Vec<String> = Vec::new();

        // Since we might encounter jumps, we'll keep track of our position
        // locally as opposed to using the position within the struct. This
        // allows us to move the shared position to a point past our current
        // qname, while keeping track of our progress on the current qname
        // using this variable.
        let mut pos = self.pos();

        // Keep track of whether we jumped or not
        let mut jumped = false;
        let mut jumps_performed = 0;

        loop {
            // Dns Packets are untrusted data, so we need to be paranoid. Someone
            // can craft a packet with a cycle in the jump instructions. This guards
            // against such packets.
            if jumps_performed > MAX_JUMPS {
                return Err(format!("Limit of {} jumps exceeded", MAX_JUMPS).into());
            }

            // ASSUMPTION: `read_qname` is always called when internal pointer
            // (`pos`) is at begining of a local.
            let len = self.get(pos)?;

            // If `len` has the two most significant bits are set, it represents
            // a jump to some other offset in the packet.
            if (len & 0xC0) == 0xC0 {
                // Update the buffer position to a point past the current
                // label. We don't need to touch it any further.
                if !jumped {
                    self.seek(pos + 2)?;
                }

                // Read next byte, calculate offset and perform the jump by
                // updating our local position.
                let b2 = self.get(pos + 1)? as u16;
                let offset = (((len as u16) ^ 0xC0) << 8) | b2;

                pos = offset as usize;

                // Indicate that a jump was performed.
                jumped = true;
                jumps_performed += 1;

                continue;
            }
            // The base scenario, where we are reading a single label and
            // appending it to the label vector.
            else {
                // Move a single byte forward to move past the length byte.
                pos += 1;

                // Domain name are terminated by an emoty label of length 0 (NUL
                // byte), so if the length is  zero we are done.
                if len == 0 {
                    break;
                }

                // Extract the actual ASCII bytes for this label and append them
                // to the output buffer.
                let str_buffer = self.get_range(pos, len as usize)?;

                labels.push(String::from_utf8_lossy(str_buffer).to_lowercase());

                // Move forward the full length of the label.
                pos += len as usize;
            }
        }

        if !jumped {
            self.seek(pos)?;
        }

        *outstr = labels.join(".");

        Ok(())
    }

    pub fn write_u8(&mut self, val: u8) -> Result<()> {
        if self.pos >= 512 {
            return Err("End of buffer".into());
        }

        self.buf[self.pos] = val;
        self.pos += 1;

        Ok(())
    }

    pub fn write_u16(&mut self, val: u16) -> Result<()> {
        self.write_u8((val >> 8) as u8)?;
        self.write_u8((val & 0xFF) as u8)?;

        Ok(())
    }

    pub fn write_u32(&mut self, val: u32) -> Result<()> {
        self.write_u8(((val >> 24) & 0xFF) as u8)?;
        self.write_u8(((val >> 16) & 0xFF) as u8)?;
        self.write_u8(((val >> 8) & 0xFF) as u8)?;
        self.write_u8((val & 0xFF) as u8)?;

        Ok(())
    }

    pub fn write_qname(&mut self, qname: &str) -> Result<()> {
        for label in qname.split('.') {
            let len = label.len();

            if len > 0x3f {
                return Err("Single label exceeds 63 characters of length".into());
            }

            self.write_u8(len as u8)?;

            for b in label.as_bytes() {
                self.write_u8(*b)?;
            }
        }

        self.write_u8(0)?;

        Ok(())
    }

    fn set(&mut self, pos: usize, val: u8) -> Result<()> {
        self.buf[pos] = val;

        Ok(())
    }

    pub fn set_u16(&mut self, pos: usize, val: u16) -> Result<()> {
        self.set(pos, (val >> 8) as u8)?;
        self.set(pos + 1, (val & 0xFF) as u8)?;

        Ok(())
    }
}

impl Default for BytePacketBuffer {
    fn default() -> Self {
        Self {
            // Holds the raw DNS packet information.
            buf: [0; 512],

            // Keep track of the read/write position of the buffer.
            pos: 0,
        }
    }
}
