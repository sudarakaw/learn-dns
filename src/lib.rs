use std::error::Error;

pub mod io;
pub mod net;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;
