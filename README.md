# Build Simple DNS Server

Sample code written while following the [Building a DNS server in Rust](https://github.com/EmilHernvall/dnsguide) guide by [Emil Hernvall](https://github.com/EmilHernvall).

